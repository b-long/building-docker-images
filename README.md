# Building Docker images in GitLab CI

Demonstration of building Docker images in GitLab CI using Docker-in-Docker and Podman.

Want to learn more Docker packaging techniques for production, with a focus on Python? Visit [Production-ready Docker packaging for Python developers](https://pythonspeed.com/docker/).

## Gitlab CI issues using Podman

Podman is a "_tool for managing OCI containers and pods._" .  A more verbose description is provided in the official
Podman [documentation](https://docs.podman.io/en/latest/):

> Podman is a daemonless, open source, Linux native tool designed to make it easy to find, run, build, share and deploy
> applications using Open Containers Initiative (OCI) Containers and Container Images. Podman provides a command line 
> interface (CLI) familiar to anyone who has used the Docker Container Engine.

Basically, it is a drop-in replacement for Docker.  You can learn more about it at: https://github.com/containers/podman .

Part of the Gitlab CI pipeline for this repository will randomly fail, due to a Gitlab CI bug.  More
information about it `"Random registry push failures due to ActiveRecord::RecordInvalid (Validation failed: Name has already been taken)"` is
available at: https://gitlab.com/gitlab-org/gitlab/-/issues/215715

You can see the failure details for yourself visiting the builds page for the commit d048174666eb98311789e03397127ea88466ac85 : 
https://gitlab.com/b-long/building-docker-images/-/pipelines/398355176/builds

**Note:** the pipeline above fails randomly, so it has succeeded once and failed once to execute the same CI work
for the same commit. 

## Upstream

This project was forked from https://gitlab.com/pythonspeed/building-docker-images .  

Additional information and context is available at: https://pythonspeed.com/articles/gitlab-build-docker-image/ .
